import { useNavigate } from "react-router-dom"

function NoPage() {

    let navigate = useNavigate();
    const code = '{ 4 0 4 }';

    return (
        <section className="vh-100">
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col col-lg-7">
                        <div className="row g-0">
                            <div className="col-lg-12 col-12 text-center">
                                <h3>HALAMAN TIDAK DITEMUKAN</h3>
                                <h3>{code}</h3>
                                <br></br>
                                <button onClick={() => navigate(-1)}>Kembali</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
export default NoPage;