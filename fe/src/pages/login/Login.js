import React, { useState } from 'react';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';

function Login() {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    let navigate = useNavigate();

    const [usernameClass, setUsernameClass] = useState("form-control");
    const [passwordClass, setPasswordClass] = useState("form-control");

    const submit = () => {
        if (username && password) {
            var data = {
                username: username,
                password: password
            }
            axios.post('http://localhost:3001/api/login', data).then((res) => {
                if (res.status == 200) {
                    if (res.data.error) {
                        Swal.fire({
                            title: "Kalkulator MERN",
                            text: "Username atau Password anda salah!!!",
                            type: "error",
                            icon: "error",
                            confirmButtonColor: "#27cfc3",
                            confirmButtonText: "OK"
                        });
                    } else {
                        localStorage.setItem('data', JSON.stringify(res.data));
                        navigate("/home");
                    }
                }
            }).catch((err) => {
                Swal.fire({
                    title: "Kalkulator MERN",
                    text: err.message,
                    type: "error",
                    icon: "error",
                    confirmButtonColor: "#27cfc3",
                    confirmButtonText: "OK"
                });
                console.log(err);
            });
        } else {
            if (!username) {
                setUsernameClass("form-control is-invalid");
            }
            if (!password) {
                setPasswordClass("form-control is-invalid");
            }
        }
    }

    const setValue = (e, type) => {
        if ("username" === type) {
            if (e.target.value) {
                setUsername(e.target.value);
                setUsernameClass("form-control");
            } else {
                setUsernameClass("form-control is-invalid");
            }
        }
        if ("password" === type) {
            if (e.target.value) {
                setPassword(e.target.value);
                setPasswordClass("form-control");
            } else {
                setPasswordClass("form-control is-invalid");
            }
        }
    }

    const singup = () => {
        navigate("/signup");
    }

    return (
        <section className="vh-100">
            <div className="container py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col col-lg-5">
                        <div className="card bordered elevation-5 in-left">
                            <div className="row g-0">
                                <div className="col-lg-12 col-12 d-flex align-items-center">
                                    <div className="card-body p-4 p-lg-5 text-black">
                                        <div className="login-logo">
                                            <img src={process.env.PUBLIC_URL + '/dist/img/logo.png'} style={{ width: '20%', marginLeft: '-10px' }} />
                                            <a><b>&nbsp;Kalkulator</b> MERN</a>
                                        </div>
                                        <p className="login-box-msg">Selamat Datang, silahkan masuk</p>
                                        <div className="input-group mb-3">
                                            <input type="text" className={usernameClass} placeholder="Username" onChange={(e) => { setValue(e, "username") }} />
                                            <div className="input-group-append">
                                                <div className="input-group-text">
                                                    <span className="fas fa-user" />
                                                </div>
                                            </div>
                                            <span className="error invalid-feedback">Masukkan username</span>
                                        </div>
                                        <div className="input-group mb-3">
                                            <input type="password" className={passwordClass} placeholder="Password" onChange={(e) => { setValue(e, "password") }} />
                                            <div className="input-group-append">
                                                <div className="input-group-text">
                                                    <span className="fas fa-lock" />
                                                </div>
                                            </div>
                                            <span className="error invalid-feedback">Masukkan password</span>
                                        </div>
                                        <div className="row">
                                            <div className="col-12">
                                                <button onClick={submit} className="btn btn-primary btn-block">Masuk</button>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col offset-lg-6 col-lg-6 col-12">
                                                <a onClick={singup} className='float-right' style={{ marginTop: '20px', cursor: 'pointer', color: '#f78c27' }}>Daftar Akun Baru</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Login;