import React, { useState } from 'react';
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';

function Home() {

    const [screen, setScreen] = useState("");
    const [result, setResult] = useState("");
    const [move, setMove] = useState(false);
    const [terbilang, setTerbilang] = useState("");
    const [activeTerbilang, setActiveTerbilang] = useState(false);

    const [terbilangClass, setTerbilangClass] = useState("card-body number");

    let navigate = useNavigate();

    const calculate = (data) => {
        if ("C" === data) {
            setScreen("");
            setResult("");
            setTerbilang("");
        } else {
            if ("T" === data) {
                if (activeTerbilang) {
                    setActiveTerbilang(false);
                    setTerbilangClass("card-body number");
                } else {
                    setActiveTerbilang(true);
                    setTerbilangClass("card-body terbilang");
                }

                setTerbilang(convertTerbilang(screen));
            } else if ("D" === data) {
                var del = screen.substring(0, screen.length - 1);
                setScreen(del);
            } else {
                if ("=" === data) {
                    var a = result.substring(0, result.length - 1);
                    var b = screen;
                    var opr = result.substring(result.length - 1, result.length);
                    var value = 0;
                    if ("÷" === opr) {
                        value = Number(a) / Number(b);
                    }
                    if ("×" === opr) {
                        value = Number(a) * Number(b);
                    }
                    if ("−" === opr) {
                        value = Number(a) - Number(b);
                    }
                    if ("+" === opr) {
                        value = Number(a) + Number(b);
                    }
                    if ("%" === opr) {
                        value = Number(b) / 100;
                    }
                    setScreen(value);
                    if (activeTerbilang) {
                        setTerbilang(convertTerbilang(value));
                    }
                } else {
                    var operatorList = ["%", "÷", "×", "−", "+"];
                    var check = false;
                    for (var opr of operatorList) {
                        if (data === opr) {
                            check = true;
                        }
                    }
                    var temp = screen + data;
                    if (check) {
                        setResult(temp);
                        setScreen(screen);
                        setMove(true);
                    } else {
                        if (move) {
                            setScreen(data);
                            setMove(false);
                        } else {
                            setScreen(temp);
                        }
                    }
                }
            }
        }
    }

    const convertTerbilang = (value) => {
        var nilai = Math.abs(value);
        var simpanNilaiBagi = 0;
        var huruf = ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];
        var temp = "";

        if (nilai < 12) {
            temp = " " + huruf[nilai];
        }
        else if (nilai < 20) {
            temp = convertTerbilang(nilai - 10) + " Belas";
        }
        else if (nilai < 100) {
            simpanNilaiBagi = Math.floor(nilai / 10);
            temp = convertTerbilang(simpanNilaiBagi) + " Puluh" + convertTerbilang(nilai % 10);
        }
        else if (nilai < 200) {
            temp = " Seratus" + convertTerbilang(nilai - 100);
        }
        else if (nilai < 1000) {
            simpanNilaiBagi = Math.floor(nilai / 100);
            temp = convertTerbilang(simpanNilaiBagi) + " Ratus" + convertTerbilang(nilai % 100);
        }
        else if (nilai < 2000) {
            temp = " Seribu" + convertTerbilang(nilai - 1000);
        }
        else if (nilai < 1000000) {
            simpanNilaiBagi = Math.floor(nilai / 1000);
            temp = convertTerbilang(simpanNilaiBagi) + " Ribu" + convertTerbilang(nilai % 1000);
        }
        else if (nilai < 1000000000) {
            simpanNilaiBagi = Math.floor(nilai / 1000000);
            temp = convertTerbilang(simpanNilaiBagi) + " Juta" + convertTerbilang(nilai % 1000000);
        }
        else if (nilai < 1000000000000) {
            simpanNilaiBagi = Math.floor(nilai / 1000000000);
            temp = convertTerbilang(simpanNilaiBagi) + " Miliar" + convertTerbilang(nilai % 1000000000);
        }
        else if (nilai < 1000000000000000) {
            simpanNilaiBagi = Math.floor(nilai / 1000000000000);
            temp = convertTerbilang(nilai / 1000000000000) + " Triliun" + convertTerbilang(nilai % 1000000000000);
        }

        return temp;
    }

    const logout = () => {
        var dataset = localStorage.getItem("data");
        var json = JSON.parse(dataset);

        Swal.fire({
            title: "Kalkulator MERN",
            text: "Apakah anda yakin untuk keluar?",
            type: "question",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#27cfc3",
            cancelButtonColor: "#f78c27",
            confirmButtonText: "OK",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then((result) => {
            if (result.isConfirmed) {
                axios.post('http://localhost:3001/api/updatelog', json).then((res) => {
                    if (res.status == 200) {
                        localStorage.clear();
                        navigate("/login");
                    }
                }).catch((err) => {
                    console.log(err);
                });
            } else {
                Swal.fire({
                    title: "Kalkulator MERN",
                    text: "Batal Keluar",
                    type: "warning",
                    icon: "warning",
                    confirmButtonColor: "#27cfc3",
                    confirmButtonText: "OK"
                });
            }
        });
    }

    return (
        <div className="wrapper">
            <nav className="main-header navbar navbar-expand-md navbar-light elevation-3 slide-down">
                <div className="container padding">
                    <table width="100%">
                        <tbody><tr>
                            <td width="7%">
                                <img className="in-left" src={process.env.PUBLIC_URL + '/dist/img/logo.png'} style={{ width: '100%', marginLeft: '-10px' }} />
                            </td>
                            <td width="80%">

                                <h2 className="in-left"><b>Kalkulator MERN</b></h2>
                            </td>
                            <td className="text-right">
                                <i onClick={logout} style={{ cursor: 'pointer', fontSize: '20px' }} className="fas fa-sign-out-alt"></i>
                            </td>
                        </tr>
                        </tbody></table>
                </div>
            </nav>
            <div className="content-wrapper">
                <div className="content-header">
                    <div className="container">
                    </div>
                </div>
                <div className="content">
                    <div className="container">
                        <div className="row">
                            <div className="col offset-lg-3 col-lg-6 offset-1 col-10">
                                <div className="card bordered in-left">
                                    <div className="card-body bordered elevation-5">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="card">
                                                    <div className="card-body">
                                                        <h5 style={{ textAlign: 'right' }}>{result === '' ? '_' : result}</h5>
                                                        <h1 style={{ textAlign: 'right' }}>{screen === '' ? '_' : screen}</h1>
                                                        <h5>{terbilang === '' || terbilang === undefined ? '_' : terbilang}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('C') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>C</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('D') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>D</h1></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('T') }}>
                                                    <div className={terbilangClass}>
                                                        <div className="text-center"><h1>T</h1></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('÷') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>&#xf7;</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('7') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>7</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('8') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>8</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('9') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>9</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('×') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>&#xd7;</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('4') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>4</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('5') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>5</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('6') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>6</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('−') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>&#x2212;</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('1') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>1</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('2') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>2</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('3') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>3</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('+') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>&#x2b;</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">

                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('.') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>.</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card pointer" onClick={() => { calculate('0') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>0</h1></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('%') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>&#x25;</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="card operator" onClick={() => { calculate('=') }}>
                                                    <div className="card-body number">
                                                        <div className="text-center"><h1>&#x3d;</h1></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Home;