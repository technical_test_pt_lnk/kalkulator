const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const historyLoginSchema = Schema({
    username: {
        type: String,
        require: true
    },
    loginTime: {
        type: Date,
        require: true
    },
    logoutTime: {
        type: Date,
        require: true
    },
    hours: {
        type: Number,
        require: true
    },
});

module.exports = mongoose.model("log_login_data", historyLoginSchema);