const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    createdDate: {
        type: Date,
        require: true
    },
});

module.exports = mongoose.model("user_data", userSchema);


