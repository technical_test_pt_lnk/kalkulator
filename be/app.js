const express = require("express");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const cors = require("cors");
const app = express();

const UsersModel = require("./models/Users");
const HistoryLoginModel = require("./models/HistoryLogin");

app.use(express.json());
app.use(cors());

mongoose.connect("mongodb://localhost:27017/PT_LNK_TEST", { useNewUrlParser: true },
    (err) => {
        if (!err) {
            console.log('DB connection Successfully')
        }
        else {
            console.log('Error in DB Connection :' + err)
        }
    });

app.post("/api/register", async (req, res) => {
    try {
        const body = req.body;
        if (!(body.username && body.password)) {
            return res.status(400).send({ error: "Data not formatted properly" });
        }

        const userCheck = await UsersModel.findOne({ username: body.username });
        if (userCheck) {
            return res.status(200).send({ error: "Username " + body.username + " sudah terdaftar" });
        } else {
            const user = new UsersModel(body);
            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(user.password, salt);
            user.createdDate = new Date();

            const users = new UsersModel(user);
            await users.save().then((doc) => res.status(200).send(doc));
        }
    } catch (err) {
        console.log(err);
        return res.status(400).send({ error: "Internal server Error" });
    }
});

app.post("/api/updatelog", async (req, res) => {
    try {
        const body = req.body;
        if (!(body.username)) {
            return res.status(400).send({ error: "Data not formatted properly" });
        }

        const logCheck = await HistoryLoginModel.findById({ _id: body._id });
        if (logCheck) {
            logCheck.logoutTime = new Date();
            var diff = new Date().valueOf() - logCheck.loginTime.valueOf();
            var diffInHours = (diff / 1000 / 60 / 60).toFixed(2);
            logCheck.hours = diffInHours;
            await logCheck.save();
            res.status(200).json({ message: "Updated!!!" });
        }

    } catch (err) {
        console.log(err);
        return res.status(400).send({ error: "Internal server Error" });
    }
});

app.post("/api/login", async (req, res) => {
    try {
        const body = req.body;
        const user = await UsersModel.findOne({ username: body.username });
        if (user) {
            const validPassword = await bcrypt.compare(body.password, user.password);
            if (validPassword) {
                const log = new HistoryLoginModel();
                log.username = body.username;
                log.loginTime = new Date();
                log.logoutTime = null;
                log.hours = null;
                const logLogin = new HistoryLoginModel(log);
                await logLogin.save().then((coll) => {
                    res.status(200).send(coll);
                });
            } else {
                res.status(200).json({ error: "Invalid Password" });
            }
        } else {
            res.status(401).json({ error: "User does not exist" });
        }
    } catch (err) {
        console.log(err);
        return res.status(400).send({ error: "Internal server Error" });
    }
});

app.get("/api/hello", async (req, res) => {
    try {
        res.send('hello...')
    } catch (err) {
        console.log(err)
    }
});



const port = 3001;
app.listen(port, () => {
    console.log("Server running on port " + port + "...");
});